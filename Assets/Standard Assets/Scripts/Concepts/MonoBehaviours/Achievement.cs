using GridGame;
using UnityEngine;
using System.Collections.Generic;

public class Achievement : MonoBehaviour
{
	public GameObject receivedIndicator;
	public static Dictionary<string, Achievement> instancesDict = new Dictionary<string, Achievement>();

	void Awake ()
	{
		instancesDict.Add(name, this);
		if (SaveAndLoadManager.GetValue<bool>("Achievement " + name + " receieved"))
			receivedIndicator.SetActive(true);
	}

	public void Receive ()
	{
		SaveAndLoadManager.SetValue ("Achievement " + name + " receieved", true);
		receivedIndicator.SetActive(true);
	}

#if UNITY_EDITOR
	void OnDestroy ()
	{
		instancesDict.Clear();
	}
#endif
}