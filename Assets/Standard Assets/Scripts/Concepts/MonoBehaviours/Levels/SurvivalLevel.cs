using TMPro;
using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace GridGame
{
	public class SurvivalLevel : Level
	{
		public static new SurvivalLevel instance;
        public static new SurvivalLevel Instance
        {
            get
            {
                if (instance == null)
                    instance = FindObjectOfType<SurvivalLevel>();
                return instance;
            }
        }
		public EnemySpawnEntry[] enemySpawnEntries = new EnemySpawnEntry[0];
		public RectInt arenaRect;
		public TMP_Text movesMadeText;
		public TMP_Text mostMovesMadeText;
		public int enemySpawnDelay;
		public Dictionary<Enemy, int> enemiesToSpawnDict = new Dictionary<Enemy, int>();

		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			if (!enabled || !gameObject.activeInHierarchy)
				return;
			instance = this;
			Player.instance = Player.Instance;
			base.Awake ();
			mostMovesMadeText.text = "Most Moves: " + SaveAndLoadManager.saveData.mostMovesMade;
			Player.instance.onActed += OnPlayerActed;
			OnPlayerActed ();
		}

		void OnPlayerActed ()
		{
			movesMadeText.text = "Moves: " + Player.movesMade;
			if (Player.movesMade > SaveAndLoadManager.saveData.mostMovesMade)
			{
				SaveAndLoadManager.saveData.mostMovesMade = (uint) Player.movesMade;
				mostMovesMadeText.text = "Most Moves: " + Player.movesMade;
			}
			Enemy[] enemiesToSpawn = new Enemy[enemiesToSpawnDict.Count];
			enemiesToSpawnDict.Keys.CopyTo(enemiesToSpawn, 0);
			for (int i = 0; i < enemiesToSpawn.Length; i ++)
			{
				Enemy enemy = enemiesToSpawn[i];
				int spawnDelay = enemiesToSpawnDict[enemy];
				enemy.spriteRenderer.color = enemy.spriteRenderer.color.AddAlpha(1f / (enemySpawnDelay + 1));
				if (spawnDelay == 1)
				{
					if (Player.instance.trs.position == enemy.trs.position)
					{
						Player.instance.Death ();
						return;
					}
					enemy.enabled = true;
					enemiesToSpawnDict.Remove(enemy);
				}
				else
					enemiesToSpawnDict[enemy] --;
			}
			for (int i = 0; i < enemySpawnEntries.Length; i ++)
			{
				EnemySpawnEntry enemySpawnEntry = enemySpawnEntries[i];
				if (Player.movesMade >= enemySpawnEntry.movesUntilSpawn)
				{
					SpawnEnemy (enemySpawnEntry);
					enemySpawnEntry.spawns ++;
					if (enemySpawnEntry.spawns == enemySpawnEntry.spawnsUntilDecreaseSpawnInterval && enemySpawnEntry.spawnInterval > 1)
					{
						enemySpawnEntry.spawnInterval -= enemySpawnEntry.decreaseSpawnInverval;
						enemySpawnEntry.spawnsUntilDecreaseSpawnInterval += enemySpawnEntry.increaseSpawnsUntilDecreaseSpawnInterval;
						enemySpawnEntry.increaseSpawnsUntilDecreaseSpawnInterval += enemySpawnEntry.increaseSpawnsUntilDecreaseSpawnInvervalIncrease;
					}
					enemySpawnEntry.movesUntilSpawn += enemySpawnEntry.spawnInterval;
					enemySpawnEntries[i] = enemySpawnEntry;
				}
			}
		}

		Enemy SpawnEnemy (EnemySpawnEntry enemySpawnEntry)
		{
			Vector2 spawnPosition;
			do
			{
				spawnPosition = new Vector2(Random.Range(arenaRect.xMin, arenaRect.xMax), Random.Range(arenaRect.yMin, arenaRect.yMax));
			} while ((Player.instance.trs.position - (Vector3) spawnPosition).sqrMagnitude < enemySpawnEntry.minSpawnDistanceToPlayer * enemySpawnEntry.minSpawnDistanceToPlayer);
			Enemy enemy = Instantiate(enemySpawnEntry.enemyPrefab, spawnPosition, Quaternion.identity);
			if (enemy is not RandomEnemy && enemySpawnDelay > 0)
			{
				enemy.enabled = false;
				enemy.spriteRenderer.color = enemy.spriteRenderer.color.SetAlpha(1f / (enemySpawnDelay + 1));
				enemiesToSpawnDict.Add(enemy, enemySpawnDelay);
			}
			int randomValue = Random.Range(0, 4);
			if (randomValue == 0)
				enemy.trs.up = Vector3.right;
			else if (randomValue == 1)
				enemy.trs.up = Vector3.up;
			else if (randomValue == 2)
				enemy.trs.up = Vector3.left;
			else// if (randomValue == 3)
				enemy.trs.up = Vector3.down;
			return enemy;
		}
		
		[Serializable]
		public struct EnemySpawnEntry
		{
			public Enemy enemyPrefab;
			public float minSpawnDistanceToPlayer;
			public int spawnInterval;
			public int movesUntilSpawn;
			public int decreaseSpawnInverval;
			public int spawnsUntilDecreaseSpawnInterval;
			public int increaseSpawnsUntilDecreaseSpawnInterval;
			public int increaseSpawnsUntilDecreaseSpawnInvervalIncrease;
			public int spawns;
		}
	}
}