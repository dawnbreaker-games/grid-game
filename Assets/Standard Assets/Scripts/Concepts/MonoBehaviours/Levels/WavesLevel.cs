using TMPro;
using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace GridGame
{
	public class WavesLevel : Level
	{
		public EnemySpawnEntry[] enemySpawnEntries = new EnemySpawnEntry[0];
		public RectInt arenaRect;
		public int currentDifficulty;
		public int difficultyPerWave;
		public int currentWave;
		public TMP_Text currentWaveText;
		public TMP_Text highestWaveReachedText;
		int enemiesRemaining;

		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			if (!enabled || !gameObject.activeInHierarchy)
				return;
			Player.instance = Player.Instance;
			base.Awake ();
			highestWaveReachedText.text = "Best wave: " + SaveAndLoadManager.saveData.bestWaveReached;
			NextWave ();
		}

		void NextWave ()
		{
			currentWave ++;
			currentWaveText.text = "Wave: " + currentWave;
			if (currentWave > SaveAndLoadManager.saveData.bestWaveReached)
			{
				SaveAndLoadManager.saveData.bestWaveReached = (uint) currentWave;
				highestWaveReachedText.text = "Best wave: " + currentWave;
			}
			currentDifficulty += difficultyPerWave;
			float remainingDifficulty = currentDifficulty;
			List<EnemySpawnEntry> remainingEnemySpawnEntries = new List<EnemySpawnEntry>(enemySpawnEntries);
			while (remainingDifficulty > 0)
			{
				int enemySpawnEntryIndex = Random.Range(0, remainingEnemySpawnEntries.Count);
				EnemySpawnEntry enemySpawnEntry = remainingEnemySpawnEntries[enemySpawnEntryIndex];
				if (enemySpawnEntry.difficulty <= remainingDifficulty)
				{
					remainingDifficulty -= enemySpawnEntry.difficulty;
					SpawnEnemy (enemySpawnEntry);
				}
				else
				{
					remainingEnemySpawnEntries.RemoveAt(enemySpawnEntryIndex);
					if (remainingEnemySpawnEntries.Count == 0)
						return;
				}
			}
		}

		Enemy SpawnEnemy (EnemySpawnEntry enemySpawnEntry)
		{
			Vector2 spawnPosition;
			do
			{
				spawnPosition = new Vector2(Random.Range(arenaRect.xMin, arenaRect.xMax), Random.Range(arenaRect.yMin, arenaRect.yMax));
			} while ((Player.instance.trs.position - (Vector3) spawnPosition).sqrMagnitude < enemySpawnEntry.minSpawnDistanceToPlayer * enemySpawnEntry.minSpawnDistanceToPlayer);
			Enemy enemy = Instantiate(enemySpawnEntry.enemyPrefab, spawnPosition, Quaternion.identity);
			int randomValue = Random.Range(0, 4);
			if (randomValue == 0)
				enemy.trs.up = Vector3.right;
			else if (randomValue == 1)
				enemy.trs.up = Vector3.up;
			else if (randomValue == 2)
				enemy.trs.up = Vector3.left;
			else// if (randomValue == 3)
				enemy.trs.up = Vector3.down;
			enemy.onDeath = OnDeath;
			enemiesRemaining ++;
			return enemy;
		}
		
		void OnDeath ()
		{
			enemiesRemaining --;
			if (enemiesRemaining == 0)
				NextWave ();
		}

		[Serializable]
		public struct EnemySpawnEntry
		{
			public Enemy enemyPrefab;
			public float difficulty;
			public float minSpawnDistanceToPlayer;
		}
	}
}