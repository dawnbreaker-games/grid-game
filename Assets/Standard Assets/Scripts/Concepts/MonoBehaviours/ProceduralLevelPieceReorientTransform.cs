using Extensions;
using UnityEngine;

namespace GridGame
{
	public class ProceduralLevelPieceReorientTransform : MonoBehaviour
	{
		public Transform trs;
		public BoxCollider reorientBoundsCollider;
		public BoxCollider childrenBoundsCollider;
		public Transform[] possibleOrientations = new Transform[0];
		public RepositionType positionType;
		public RerotateType rotateType;

		public void Do ()
		{
			if (rotateType == RerotateType.Transforms)
				trs.rotation = possibleOrientations[Random.Range(0, possibleOrientations.Length)].rotation;
			if (positionType == RepositionType.BoxColliderBounds)
				trs.position = reorientBoundsCollider.bounds.Shrink(childrenBoundsCollider.bounds).RandomPoint().Snap(Vector3.one);
			else
				trs.position = possibleOrientations[Random.Range(0, possibleOrientations.Length)].position;
		}

		public enum RepositionType
		{
			BoxColliderBounds,
			Transforms
		}

		public enum RerotateType
		{
			DontRerotate,
			Transforms
		}
	}
}