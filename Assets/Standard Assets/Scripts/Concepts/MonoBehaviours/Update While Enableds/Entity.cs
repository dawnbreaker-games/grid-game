using System;
using UnityEngine;

namespace GridGame
{
	public class Entity : UpdateWhileEnabled, IDestructable
	{
		float hp;
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public int maxHp;
		public int MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		public Transform trs;
		bool isDead;

		public override void OnEnable ()
		{
			base.OnEnable ();
			hp = maxHp;
		}

		public virtual void TakeDamage (float amount = 1)
		{
			hp -= amount;
			if (hp <= 0 && !isDead)
			{
				isDead = true;
				Death ();
			}
		}

		public virtual void Death ()
		{
		}

		public virtual void RotateToPlayer ()
		{
			int xOffseToPlayer = (int) (Player.instance.trs.position.x - trs.position.x);
			int yOffseToPlayer = (int) (Player.instance.trs.position.y - trs.position.y);
			int xDistanceToPlayer = Mathf.Abs(xOffseToPlayer);
			int yDistanceToPlayer = Mathf.Abs(yOffseToPlayer);
			if (xDistanceToPlayer > yDistanceToPlayer)
				trs.up = Vector3.right * xOffseToPlayer;
			else if (xDistanceToPlayer < yDistanceToPlayer)
				trs.up = Vector3.up * yOffseToPlayer;
			else
			{
				int xDirectionToPlayer = (int) Mathf.Sign(xOffseToPlayer);
				int yDirectionToPlayer = (int) Mathf.Sign(yOffseToPlayer);
				if (trs.up.x != xDirectionToPlayer && trs.up.y != yDirectionToPlayer)
				{
					if (Vector2.Angle(trs.up, Vector3.right * xDirectionToPlayer) <= 91)
						trs.up = Vector3.right * xDirectionToPlayer;
					else
						trs.up = Vector3.up * yDirectionToPlayer;
				}
			}
		}
	}
}