using Extensions;
using UnityEngine;

namespace GridGame
{
	public class Wall : MonoBehaviour
	{
		public static Wall[] instances = new Wall[0];
        public Transform trs;

		void OnEnable ()
		{
			instances = instances.Add(this);
		}

		void OnDisable ()
		{
			instances = instances.Remove(this);
		}
	}
}