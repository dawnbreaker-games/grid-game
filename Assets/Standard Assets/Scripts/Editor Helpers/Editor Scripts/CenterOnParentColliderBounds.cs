#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using Extensions;

namespace GridGame
{
	public class CenterOnParentColliderBounds : EditorScript
	{
		public Transform trs;

		public override void Do ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
			_Do (trs);
		}

		public static void _Do (Transform trs)
		{
			Collider[] colliders = trs.GetComponentsInParent<Collider>();
			Bounds[] collidersBounds = new Bounds[colliders.Length];
			for (int i = 0; i < colliders.Length; i ++)
			{
				Collider collider = colliders[i];
				collidersBounds[i] = collider.bounds;
			}
			Bounds bounds = collidersBounds.Combine();
			trs.position = bounds.center;
		}

		[MenuItem("Tools/Center selected children on parent collider bounds")]
		static void _Do ()
		{
			Transform[] selectedTransforms = Selection.transforms;
			for (int i = 0; i < selectedTransforms.Length; i ++)
			{
				Transform selectedTrs = selectedTransforms[i];
				_Do (selectedTrs);
			}
		}
	}
}
#else
namespace GridGame
{
	public class CenterOnParentColliderBounds : EditorScript
	{
	}
}
#endif