#if UNITY_EDITOR
using System;
using UnityEngine;

namespace GridGame
{
	public class ChangeSceneMaterials : EditorScript
	{
		public Renderer[] renderers = new Renderer[0];
		public MaterialChangeEntry[] materialChangeEntries = new MaterialChangeEntry[0];

		public override void Do ()
		{
			if (renderers.Length == 0)
				renderers = FindObjectsOfType<Renderer>();
			_Do (renderers, materialChangeEntries);
		}

		public static void _Do (Renderer[] renderers, MaterialChangeEntry[] materialChangeEntries)
		{
			for (int i = 0; i < renderers.Length; i ++)
			{
				Renderer renderer = renderers[i];
				for (int i2 = 0; i2 < materialChangeEntries.Length; i2 ++)
				{
					MaterialChangeEntry materialChangeEntry = materialChangeEntries[i2];
					if (renderer.sharedMaterial == materialChangeEntry.oldMaterial)
					{
						renderer.sharedMaterial = materialChangeEntry.newMaterial;
						TileRendererMaterial tileRendererMaterial = renderer.GetComponent<TileRendererMaterial>();
						if (tileRendererMaterial != null)
							tileRendererMaterial.Do ();
						renderer.enabled = !renderer.enabled;
						renderer.enabled = !renderer.enabled;
						break;
					}
				}
			}
		}

		[Serializable]
		public struct MaterialChangeEntry
		{
			public Material oldMaterial;
			public Material newMaterial;
		}
	}
}
#else
namespace GridGame
{
	public class ChangeSceneMaterials : EditorScript
	{
	}
}
#endif