﻿#if UNITY_EDITOR
using System;
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using UnityEditor.SceneManagement;

namespace GridGame
{
	public class DestroyGameObjectsWithSameTransformAndPrefabSource : EditorScript
	{
		public GameObject[] gos = new GameObject[0];

		public override void Do ()
		{
			if (this == null)
				return;
			_Do (gos);
		}

		static void _Do (GameObject[] gos)
		{
			Dictionary<GameObject, Transform> goSourcesDict = new Dictionary<GameObject, Transform>();
			for (int i = 0; i < gos.Length; i ++)
			{
				GameObject go = gos[i];
				Transform trs = go.GetComponent<Transform>();
				Transform otherTrs;
				GameObject sourceGo = PrefabUtility.GetCorrespondingObjectFromSource(go);
				if (sourceGo != null)
				{
					if (goSourcesDict.TryGetValue(sourceGo, out otherTrs))
					{
						if (trs.IsSameOrientationAndScale(otherTrs))
							DestroyImmediate(go);
					}
					else
						goSourcesDict.Add(sourceGo, trs);
				}
			}
		}

		[MenuItem("Tools/Destroy selected GameObjects with same Transform and prefab source")]
		static void _DoToSelected ()
		{
			_Do (Selection.gameObjects);
		}

		[MenuItem("Tools/Destroy all GameObjects with same Transform and prefab source")]
		static void _DoToAll ()
		{
			_Do (FindObjectsOfType<GameObject>());
		}
	}
}
#else
namespace GridGame
{
	public class DestroyGameObjectsWithSameTransformAndPrefabSource : EditorScript
	{
	}
}
#endif