#if UNITY_EDITOR
using System;
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace GridGame
{
	public class SubdivideMesh : EditorScript
	{
		public MeshFilter[] meshFilters = new MeshFilter[0];
		public bool useSharedMesh;
		public int level;

		public override void Do ()
		{
			for (int i = 0; i < meshFilters.Length; i ++)
			{
				MeshFilter meshFilter = meshFilters[i];
				_Do (meshFilter, useSharedMesh, level);
			}
		}

		static void _Do (MeshFilter meshFilter, bool useSharedMesh, int level)
		{
			if (useSharedMesh)
				_Do (meshFilter.sharedMesh, level);
			else
				_Do (meshFilter.mesh, level);
		}

		static void _Do (Mesh mesh, int level)
		{
			mesh.Subdivide (level);
		}

		[MenuItem("Tools/Subdivide selected MeshFilters' shared meshes")]
		static void _DoToSharedMeshesOfSelectedMeshFilters ()
		{
			MeshFilter[] meshFilters = SelectionExtensions.GetSelected<MeshFilter>();
			for (int i = 0; i < meshFilters.Length; i ++)
			{
				MeshFilter meshFilter = meshFilters[i];
				_Do (meshFilter, true, 2);
			}
		}

		[MenuItem("Tools/Subdivide selected MeshFilters' meshes")]
		static void _DoToMeshesOfSelectedMeshFilters ()
		{
			MeshFilter[] meshFilters = SelectionExtensions.GetSelected<MeshFilter>();
			for (int i = 0; i < meshFilters.Length; i ++)
			{
				MeshFilter meshFilter = meshFilters[i];
				_Do (meshFilter, false, 2);
			}
		}
	}
}
#else
namespace GridGame
{
	public class SubdivideMesh : EditorScript
	{
	}
}
#endif