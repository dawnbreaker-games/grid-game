﻿using System;
using System.Net;
using System.Web;
using UnityEngine;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace Extensions
{
	public static class StringExtensions
	{
		public static string SubstringStartEnd (this string str, int startIndex, int endIndex)
		{
			return str.Substring(startIndex, endIndex - startIndex);
		}
		
		public static string RemoveEach (this string str, string remove)
		{
			return str.Replace(remove, "");
		}
		
		public static string StartAfter (this string str, string startAfter)
		{
			return str.Substring(str.IndexOf(startAfter) + startAfter.Length);
		}

		public static string RemoveStartEnd (this string str, int startIndex, int endIndex)
		{
			return str.Remove(startIndex, endIndex - startIndex);
		}

		public static string RemoveStartAt (this string str, string remove)
		{
			return str.Remove(str.IndexOf(remove));
		}

		public static string RemoveAfter (this string str, string remove)
		{
			return str.Remove(str.IndexOf(remove) + remove.Length);
		}

		public static string RemoveStartEnd (this string str, string startString, string endString)
		{
			string output = str;
			int indexOfStartString = str.IndexOf(startString);
			if (indexOfStartString != -1)
			{
				string startOfStr = str.Substring(0, indexOfStartString);
				str = str.Substring(indexOfStartString + startString.Length);
				output = startOfStr + str.RemoveStartEnd(0, str.IndexOf(endString) + endString.Length);
			}
			return output;
		}

		public static string Translate (this string textInput, string languageCodeInput, string languageCodeOutput)
		{
			string url = string.Format("https://translate.googleapis.com/translate_a/single?client=gtx&sl={0}&tl={1}&dt=t&q={2}", languageCodeInput, languageCodeOutput, textInput.UrlEncode());
			WebClient webClient = new WebClient { Encoding = Encoding.UTF8 };
			string result = webClient.DownloadString(url);
			return result.Substring(4, result.IndexOf("\",\"", 4, StringComparison.Ordinal) - 4);
		}

		public static bool TryToTranslate (this string textInput, string languageCodeInput, string languageCodeOutput, ref string textOutput)
		{
			try
			{
				string url = string.Format("https://translate.googleapis.com/translate_a/single?client=gtx&sl={0}&tl={1}&dt=t&q={2}", languageCodeInput, languageCodeOutput, textInput.UrlEncode());
				WebClient webClient = new WebClient { Encoding = Encoding.UTF8 };
				string result = webClient.DownloadString(url);
				textOutput = result.Substring(4, result.IndexOf("\",\"", 4, StringComparison.Ordinal) - 4);
				return true;
			}
			catch
			{
				return false;
			}
		}

		public static string UrlEncode (this string text)
		{
			return text.Replace(" ", "+").Replace(",", "%2c").Replace(":", "%3a").Replace(";", "%3b").Replace("?", "%3f").Replace("'", "%27");
		}
	}
}