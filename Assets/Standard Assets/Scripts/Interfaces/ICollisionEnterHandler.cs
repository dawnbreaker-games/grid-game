using UnityEngine;

namespace GridGame
{
	public interface ICollisionEnterHandler
	{
        Collider Collider { get; }
        
        void OnCollisionEnter (Collision coll);
	}
}