using UnityEngine;

namespace GridGame
{
	public interface ICollisionExitHandler
	{
        Collider Collider { get; }
        
        void OnCollisionExit (Collision coll);
	}
}