﻿﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GridGame
{
	public class AudioManager : SingletonMonoBehaviour<AudioManager>
	{
		public SoundEffect soundEffectPrefab;
		
		public SoundEffect MakeSoundEffect (AudioClip audioClip, Vector3 position)
		{
			return MakeSoundEffect(audioClip, position, soundEffectPrefab.settings.Volume);
		}
		
		public SoundEffect MakeSoundEffect (AudioClip audioClip, Vector3 position, float volume)
		{
			SoundEffect.Settings soundEffectSettings = soundEffectPrefab.settings;
			soundEffectSettings.audioClip = audioClip;
			soundEffectSettings.Position = position;
			soundEffectSettings.Volume = volume;
			return MakeSoundEffect(soundEffectSettings);
		}
		
		public SoundEffect MakeSoundEffect (SoundEffect.Settings soundEffectSettings)
		{
			SoundEffect output = ObjectPool.instance.SpawnComponent<SoundEffect>(soundEffectPrefab.prefabIndex, soundEffectSettings.Position, soundEffectSettings.Rotation);
			output.settings = soundEffectSettings;
			output.Play();
			return output;
		}
		
		public IEnumerator MakeSoundEffectWithDelay (AudioClip audioClip, Transform trs, float volume, float delay)
		{
			Vector3 position = trs.position;
			yield return new WaitForSeconds(delay);
			if (trs == null)
				yield return MakeSoundEffect (audioClip, position, volume);
			else
				yield return MakeSoundEffect (audioClip, trs.position, volume);
		}
	}
}