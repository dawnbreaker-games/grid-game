﻿using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine.InputSystem.LowLevel;
using TouchPhase = UnityEngine.InputSystem.TouchPhase;

namespace GridGame
{
	public class InputManager : SingletonUpdateWhileEnabled<InputManager>
	{
		public InputDevice inputDevice;
		public static InputDevice _InputDevice
		{
			get
			{
				return Instance.inputDevice;
			}
		}
		public InputSettings settings;
		public static InputSettings Settings
		{
			get
			{
				return Instance.settings;
			}
		}
		public static bool UsingGamepad
		{
			get
			{
				return Gamepad.current != null;
			}
		}
		public static bool UsingMouse
		{
			get
			{
				return Mouse.current != null;
			}
		}
		public static bool UsingKeyboard
		{
			get
			{
				return Keyboard.current != null;
			}
		}
		public static bool LeftClickInput
		{
			get
			{
				return Mouse.current.leftButton.isPressed;
			}
		}
		public bool _LeftClickInput
		{
			get
			{
				return LeftClickInput;
			}
		}
		public static Vector2? MousePosition
		{
			get
			{
				return Mouse.current.position.ReadValue();
			}
		}
		public Vector2? _MousePosition
		{
			get
			{
				return MousePosition;
			}
		}
		public static bool SubmitInput
		{
			get
			{
				return Keyboard.current.enterKey.isPressed;// || Mouse.current.leftButton.isPressed;
			}
		}
		public bool _SubmitInput
		{
			get
			{
				return SubmitInput;
			}
		}
		public static Vector2 UIMovementInput
		{
			get
			{
				int x = 0;
				if (Keyboard.current.dKey.isPressed || Keyboard.current.rightArrowKey.isPressed)
					x ++;
				if (Keyboard.current.aKey.isPressed || Keyboard.current.leftArrowKey.isPressed)
					x --;
				int y = 0;
				if (Keyboard.current.wKey.isPressed || Keyboard.current.upArrowKey.isPressed)
					y ++;
				if (Keyboard.current.sKey.isPressed || Keyboard.current.downArrowKey.isPressed)
					y --;
				return Vector2.ClampMagnitude(new Vector2(x, y), 1);
			}
		}
		public Vector2 _UIMovementInput
		{
			get
			{
				return UIMovementInput;
			}
		}
		public static Vector2 MoveInput
		{
			get
			{
				Vector2 output = new Vector2();
				if (UsingKeyboard)
				{
					if (Keyboard.current.dKey.isPressed || Keyboard.current.rightArrowKey.isPressed)
						output = Vector2.right;
					else if (Keyboard.current.aKey.isPressed || Keyboard.current.leftArrowKey.isPressed)
						output = Vector2.left;
					else if (Keyboard.current.wKey.isPressed || Keyboard.current.upArrowKey.isPressed)
						output = Vector2.up;
					else if (Keyboard.current.sKey.isPressed || Keyboard.current.downArrowKey.isPressed)
						output = Vector2.down;
				}
				else
				{
					Vector2 centerOfScreen = new Vector2(Screen.width, Screen.height) / 2;
					Vector2 offsetToCenterOfScreen = centerOfScreen - Touchscreen.current.primaryTouch.position.ReadValue();
					float xOffsetToCenterOfScreenAbs = Mathf.Abs(offsetToCenterOfScreen.x);
					float yOffsetToCenterOfScreenAbs = Mathf.Abs(offsetToCenterOfScreen.y);
					if (xOffsetToCenterOfScreenAbs > yOffsetToCenterOfScreenAbs)
						output = Vector2.right * Mathf.Sign(offsetToCenterOfScreen.x);
					else
						output = Vector2.up * Mathf.Sign(offsetToCenterOfScreen.y);
				}
				return output;
			}
		}
		public Vector2 _MoveInput
		{
			get
			{
				return MoveInput;
			}
		}
		public static bool WaitInput
		{
			get
			{
				return Keyboard.current.spaceKey.isPressed;
			}
		}
		public bool _WaitInput
		{
			get
			{
				return WaitInput;
			}
		}
		public static bool RestartInput
		{
			get
			{
				return Keyboard.current.rKey.isPressed;
			}
		}
		public bool _RestartInput
		{
			get
			{
				return RestartInput;
			}
		}
		public static bool MenuInput
		{
			get
			{
				return Keyboard.current.escapeKey.isPressed;
			}
		}
		public bool _MenuInput
		{
			get
			{
				return MenuInput;
			}
		}

		public static float GetAxis (InputControl<float> positiveButton, InputControl<float> negativeButton)
		{
			return positiveButton.ReadValue() - negativeButton.ReadValue();
		}

		public static Vector2 GetAxis2D (InputControl<float> positiveXButton, InputControl<float> negativeXButton, InputControl<float> positiveYButton, InputControl<float> negativeYButton)
		{
			Vector2 output = new Vector2();
			output.x = positiveXButton.ReadValue() - negativeXButton.ReadValue();
			output.y = positiveYButton.ReadValue() - negativeYButton.ReadValue();
			output = Vector2.ClampMagnitude(output, 1);
			return output;
		}
		
		public enum HotkeyState
		{
			Down,
			Held,
			Up
		}
		
		public enum InputDevice
		{
			KeyboardAndMouse,
			Phone
		}
	}
}