using System;
using Extensions;

namespace GridGame
{
	public class LanguageManager : SingletonMonoBehaviour<LanguageManager>
	{
		public Language[] nonDefaultLanguages = new Language[0];
		public Language defaultLanguage;
		public Language currentLanguage;
		public SerializableDictionary<string, SerializableDictionary<Language, Translation>> translationsDict = new SerializableDictionary<string, SerializableDictionary<Language, Translation>>();
#if UNITY_EDITOR
		public string[] textsToTranslate = new string[0];
		public bool addTranslations;
#endif

		void OnValidate ()
		{
#if UNITY_EDITOR
			if (addTranslations)
			{
				addTranslations = false;
				for (int i = 0; i < textsToTranslate.Length; i ++)
				{
					string text = textsToTranslate[i];
					for (int i2 = 0; i2 < nonDefaultLanguages.Length; i2 ++)
					{
						Language language = nonDefaultLanguages[i2];
						AddTranslation (language, text);
					}
				}
			}
#endif
		}

		public void AddTranslations (string text)
		{
			for (int i = 0; i < nonDefaultLanguages.Length; i ++)
			{
				LanguageManager.Language language = nonDefaultLanguages[i];
				AddTranslation (language, text);
			}
		}

		public void AddTranslation (LanguageManager.Language language, string text)
		{
			string translatedText = text.Translate("en", language.code);
			LanguageManager.Translation translation = new LanguageManager.Translation(defaultLanguage, text, language, translatedText);
			AddTranslation (translation);
			translation = new LanguageManager.Translation(language, translatedText, defaultLanguage, text);
			AddTranslation (translation);
		}

		public void AddTranslation (LanguageManager.Translation translation)
		{
			if (!translationsDict.ContainsKey(translation.startText))
			{
				SerializableDictionary<LanguageManager.Language, LanguageManager.Translation> translationDict = new SerializableDictionary<LanguageManager.Language, LanguageManager.Translation>();
				translationDict.Add(translation.endLanguage, translation);
				translationsDict.Add(translation.startText, translationDict);
			}
			else
				translationsDict[translation.startText][translation.endLanguage] = translation;
		}

		public override void Awake ()
		{
			base.Awake ();
			translationsDict.Init ();
			foreach (SerializableDictionary<Language, Translation> translationDict in translationsDict.Values)
				translationDict.Init ();
		}

		[Serializable]
		public struct Language
		{
			public string name;
			public string code;

			public Language (string name, string code)
			{
				this.name = name;
				this.code = code;
			}
		}

		[Serializable]
		public struct Translation
		{
			public Language startLanguage;
			public string startText;
			public Language endLanguage;
			public string endText;

			public Translation (Language startLanguage, string startText, Language endLanguage, string endText)
			{
				this.startLanguage = startLanguage;
				this.startText = startText;
				this.endLanguage = endLanguage;
				this.endText = endText;
			}
		}
	}
}