using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GridGame
{
	public class Bullet : MonoBehaviour
	{
		public Transform trs;
		public int damage;
		public static List<Bullet> instances = new List<Bullet>();

		void OnEnable ()
		{
			instances.Add(this);
		}

		public void Act ()
		{
			if (trs.position == Player.instance.trs.position)
				Player.instance.TakeDamage (damage);
			trs.position += trs.up;
			for (int i = 0; i < Wall.instances.Length; i ++)
			{
				Wall wall = Wall.instances[i];
				if (wall.trs.position == trs.position)
				{
					DestroyImmediate(gameObject);
					return;
				}
			}
			if (trs.position == Player.instance.trs.position)
				Player.instance.TakeDamage (damage);
		}

		void OnDisable ()
		{
			instances.Remove(this);
		}
	}
}