using Extensions;
using UnityEngine;

namespace GridGame
{
	public class Enemy1 : Enemy
	{
		public bool rotateClockwise;

		public override void Act ()
		{
			if (trs.position == Player.instance.trs.position)
			{
				Player.instance.Death ();
				return;
			}
			if (trs.position.x == Player.instance.trs.position.x || trs.position.y == Player.instance.trs.position.y)
				trs.up = Player.instance.trs.position - trs.position;
			for (int i = 0; i < Wall.instances.Length; i ++)
			{
				Wall wall = Wall.instances[i];
				if (wall.trs.position == trs.position + trs.up)
				{
					if (rotateClockwise)
						trs.up = ((Vector2) trs.up).Rotate(-90);
					else
						trs.up = ((Vector2) trs.up).Rotate(90);
					break;
				}
			}
			for (int i = 0; i < Wall.instances.Length; i ++)
			{
				Wall wall = Wall.instances[i];
				if (wall.trs.position == trs.position + trs.up)
					return;
			}
			trs.position += trs.up;
			trs.position = trs.position.Snap(Vector3.one);
			if (trs.position == Player.instance.trs.position)
				Player.instance.Death ();
		}
	}
}