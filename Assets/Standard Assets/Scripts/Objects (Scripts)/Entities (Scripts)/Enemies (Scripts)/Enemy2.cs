using Extensions;
using UnityEngine;

namespace GridGame
{
	public class Enemy2 : Enemy
	{
		public override void Act ()
		{
			if (trs.position == Player.instance.trs.position)
			{
				Player.instance.Death ();
				return;
			}
			RotateToPlayer ();
			for (int i = 0; i < Wall.instances.Length; i ++)
			{
				Wall wall = Wall.instances[i];
				if (wall.trs.position == trs.position + trs.up)
					return;
			}
			trs.position += trs.up;
			trs.position = trs.position.Snap(Vector3.one);
			if (trs.position == Player.instance.trs.position)
				Player.instance.Death ();
		}
	}
}