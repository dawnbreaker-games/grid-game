using Extensions;
using UnityEngine;

namespace GridGame
{
	public class Enemy3 : Enemy
	{
		public override void Act ()
		{
			if (trs.position == Player.instance.trs.position)
			{
				Player.instance.Death ();
				return;
			}
			RotateToPlayer ();
			for (int i = 0; i < Wall.instances.Length; i ++)
			{
				Wall wall = Wall.instances[i];
				if (wall.trs.position == trs.position + trs.up)
					return;
			}
			trs.position += trs.up;
			trs.position = trs.position.Snap(Vector3.one);
			if (trs.position == Player.instance.trs.position || trs.position + trs.up == Player.instance.trs.position)
				Player.instance.Death ();
		}

		public override bool WouldKillPlayerAtPosition (Vector2 position)
		{
			if (spriteRenderer.color.a < .9f)
				return false;
			float rotation = trs.eulerAngles.z;
			RotateToPlayer ();
			bool output = position == (Vector2) (trs.position + trs.up) || position == (Vector2) (trs.position + trs.up * 2);
			trs.eulerAngles = Vector3.forward * rotation;
			return output;
		}
	}
}