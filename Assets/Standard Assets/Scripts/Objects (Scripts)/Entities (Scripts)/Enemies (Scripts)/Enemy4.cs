using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace GridGame
{
	public class Enemy4 : Enemy
	{
		public Bullet bulletPrefab;
		public float minDistanceToPlayerToShootOrb;
		public float maxDistanceToPlayerToTeleportToOrb;
		public int maxBullets;
		public GameObject cantShootIndicator;
		public LineRenderer bulletIndicatorPrefab;
		List<LineRenderer> bulletIndicators = new List<LineRenderer>();
		List<Bullet> bullets = new List<Bullet>();

		public override void Act ()
		{
			if (trs.position == Player.instance.trs.position)
			{
				Player.instance.Death ();
				return;
			}
			RotateToPlayer ();
			float distanceToPlayer = (Player.instance.trs.position - trs.position).magnitude;
			for (int i = 0; i < bullets.Count; i ++)
			{
				Bullet bullet = bullets[i];
				if (bullet == null)
				{
					bullets.RemoveAt(i);
					DestroyImmediate(bulletIndicators[i].gameObject);
					bulletIndicators.RemoveAt(i);
					i --;
				}
				else
				{
					if ((bullet.trs.position - Player.instance.trs.position).magnitude <= maxDistanceToPlayerToTeleportToOrb || (bullet.trs.position - Player.instance.trs.position).magnitude < distanceToPlayer)
					{
						trs.position = bullet.trs.position;
						if (trs.position == Player.instance.trs.position)
						{
							Player.instance.Death ();
							return;
						}
						RotateToPlayer ();
						DestroyImmediate(bullet.gameObject);
						bullets.RemoveAt(i);
						DestroyImmediate(bulletIndicators[i].gameObject);
						bulletIndicators.RemoveAt(i);
						i --;
					}
				}
			}
			for (int i = 0; i < Wall.instances.Length; i ++)
			{
				Wall wall = Wall.instances[i];
				if (wall.trs.position == trs.position + trs.up)
					return;
			}
			trs.position += trs.up;
			trs.position = trs.position.Snap(Vector3.one);
			distanceToPlayer = (Player.instance.trs.position - trs.position).magnitude;
			if (distanceToPlayer >= minDistanceToPlayerToShootOrb && bullets.Count < maxBullets)
			{
				RotateToPlayer ();
				bullets.Add(Instantiate(bulletPrefab, trs.position + trs.up, trs.rotation));
				bulletIndicators.Add(Instantiate(bulletIndicatorPrefab));
			}
			for (int i = 0; i < bullets.Count; i ++)
			{
				Bullet bullet = bullets[i];
				LineRenderer bulletIndicator = bulletIndicators[i];
				bulletIndicator.SetPositions(new Vector3[] { trs.position, bullet.trs.position });
			}
			cantShootIndicator.SetActive(bullets.Count == maxBullets);
			if (trs.position == Player.instance.trs.position)
				Player.instance.Death ();
		}

		public override void Death ()
		{
			for (int i = 0; i < bullets.Count; i ++)
			{
				Bullet bullet = bullets[i];
				if (bullet != null)
					DestroyImmediate(bullet.gameObject);
				LineRenderer bulletIndicator = bulletIndicators[i];
				if (bulletIndicator != null)
					DestroyImmediate(bulletIndicator.gameObject);
			}
			base.Death ();
		}

		public override bool WouldKillPlayerAtPosition (Vector2 position)
		{
			if ((Vector2) trs.position == position)
				return false;
			if (base.WouldKillPlayerAtPosition(position))
				return true;
			for (int i = 0; i < bullets.Count; i ++)
			{
				Bullet bullet = bullets[i];
				if (bullet != null && ((Vector2) bullet.trs.position + (Vector2) bullet.trs.up - position).sqrMagnitude <= 1)
					return true;
			}
			return false;
		}
	}
}