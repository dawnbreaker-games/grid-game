using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace GridGame
{
	public class Enemy5 : Enemy
	{
		public Hazard hazardPrefab;
		public int hazardCount;
		public Transform[] potentialHazardPositions = new Transform[0];

		public override void Act ()
		{
			if (trs.position == Player.instance.trs.position)
			{
				Player.instance.Death ();
				return;
			}
			RotateToPlayer ();
			for (int i = 0; i < Wall.instances.Length; i ++)
			{
				Wall wall = Wall.instances[i];
				if (wall.trs.position == trs.position + trs.up)
					return;
			}
			trs.position += trs.up;
			trs.position = trs.position.Snap(Vector3.one);
			List<Transform> potentialHazardPositions = new List<Transform>(this.potentialHazardPositions);
			for (int i = 0; i < hazardCount; i ++)
			{
				int hazardPositionIndex = Random.Range(0, potentialHazardPositions.Count);
				Instantiate(hazardPrefab, potentialHazardPositions[hazardPositionIndex].position.Snap(Vector3.one), Quaternion.identity);
				potentialHazardPositions.RemoveAt(hazardPositionIndex);
			}
			if (trs.position == Player.instance.trs.position)
				Player.instance.Death ();
		}
	}
}