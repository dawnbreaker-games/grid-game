using Extensions;
using UnityEngine;

namespace GridGame
{
	public class RandomEnemy : Enemy
	{
		public Enemy[] enemies;
		
		void Start ()
		{
			int randomIndex = Random.Range(0, enemies.Length);
			Enemy enemy = Instantiate(enemies[randomIndex], trs.position, trs.rotation);
			enemy.onDeath = onDeath;
			if (SurvivalLevel.instance != null)
			{
				enemy.enabled = false;
				enemy.spriteRenderer.color = enemy.spriteRenderer.color.SetAlpha(1f / (SurvivalLevel.instance.enemySpawnDelay + 1));
				SurvivalLevel.instance.enemiesToSpawnDict.Add(enemy, SurvivalLevel.instance.enemySpawnDelay);
			}
			DestroyImmediate(gameObject);
		}
	}
}