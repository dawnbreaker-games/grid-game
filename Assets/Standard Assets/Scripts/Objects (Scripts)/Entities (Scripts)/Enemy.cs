using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace GridGame
{
	public class Enemy : Entity
	{
		public static Enemy[] instances = new Enemy[0];
		public Action onDeath;
		public SpriteRenderer spriteRenderer;
		public AudioClip[] deathSounds = new AudioClip[0];

		public override void OnEnable ()
		{
			base.OnEnable ();
			instances = instances.Add(this);
			foreach (KeyValuePair<string, Achievement> keyValuePair in Achievement.instancesDict)
			{
				EnemyAchievement enemyAchievement = keyValuePair.Value as EnemyAchievement;
				if (enemyAchievement != null && instances.Length == enemyAchievement.enemyCount)
					enemyAchievement.Receive ();
			}
		}

		public virtual void Act ()
		{
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			instances = instances.Remove(this);
		}

		public override void Death ()
		{
			if (onDeath != null)
				onDeath ();
			AudioClip deathSound = deathSounds[Random.Range(0, deathSounds.Length)];
			AudioManager.instance.MakeSoundEffect(deathSound, trs.position);
			AudioManager.instance.StartCoroutine(AudioManager.instance.MakeSoundEffectWithDelay (Player.instance.deathResponses[Random.Range(0, Player.instance.deathResponses.Length)], Player.instance.trs, 1, deathSound.length));
			DestroyImmediate(gameObject);
		}

		public virtual bool WouldKillPlayerAtPosition (Vector2 position)
		{
			return spriteRenderer.color.a >= .9f && ((Vector2) trs.position - position).sqrMagnitude == 1;
		}
	}
}