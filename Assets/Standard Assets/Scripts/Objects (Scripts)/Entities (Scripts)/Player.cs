using TMPro;
using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace GridGame
{
	public class Player : Entity
	{
		public static Player instance;
		public static Player Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Player>();
				return instance;
			}
		}
		public static int movesMade;
		public TMP_Text autoWaitTimerText;
		public float timeUntilAutoWait;
		public Action onActed;
		public GameObject[] turnIndicators = new GameObject[0];
		public AnimationEntry lowTimerAnimationEntry;
		public AnimationEntry notLowTimerAnimationEntry;
		public float lowTimerAmount;
		public AudioClip[] deathResponses = new AudioClip[0];
		public int potentiallyFatalTurnInverval;
		float autoWaitTimer;
		Vector2 previousMove;
		Vector2 move;
		bool previousWaitInput;
		bool waitInput;

		public override void OnEnable ()
		{
			base.OnEnable ();
			autoWaitTimer = timeUntilAutoWait;
			movesMade = 0;
		}

		public override void DoUpdate ()
		{
			move = InputManager.MoveInput;
			waitInput = InputManager.WaitInput;
			autoWaitTimer = Mathf.Clamp(autoWaitTimer - Time.deltaTime, 0, Mathf.Infinity);
			autoWaitTimerText.text = "Time To Make Move: " + string.Format("{0:0.#}", autoWaitTimer);
			if (autoWaitTimer <= lowTimerAmount)
				lowTimerAnimationEntry.Play ();
			else
				notLowTimerAnimationEntry.Play ();
			if ((previousMove != move && move != Vector2.zero) || (waitInput && !previousWaitInput) || autoWaitTimer == 0)
			{
				bool shouldMove = true;
				for (int i = 0; i < Wall.instances.Length; i ++)
				{
					Wall wall = Wall.instances[i];
					if (wall.trs.position == trs.position + (Vector3) move)
					{
						shouldMove = false;
						break;
					}
				}
				Vector2 previousPosition = trs.position;
				if (shouldMove)
				{
					trs.position += (Vector3) move;
					trs.position = trs.position.Snap(Vector3.one);
				}
				for (int i = 0; i < Hazard.instances.Count; i ++)
				{
					Hazard hazard = Hazard.instances[i];
					if (hazard.WouldKillPlayerAtPosition(trs.position))
					{
						trs.position = previousPosition;
						previousMove = move;
						previousWaitInput = waitInput;
						return;
					}
				}
				for (int i = 0; i < Enemy.instances.Length; i ++)
				{
					Enemy enemy = Enemy.instances[i];
					if (movesMade % potentiallyFatalTurnInverval == 0 && enemy.WouldKillPlayerAtPosition(trs.position))
					{
						trs.position = previousPosition;
						previousMove = move;
						previousWaitInput = waitInput;
						return;
					}
				}
				for (int i = 0; i < Hazard.instances.Count; i ++)
				{
					Hazard hazard = Hazard.instances[i];
					hazard.Act ();
				}
				for (int i = 0; i < Bullet.instances.Count; i ++)
				{
					Bullet bullet = Bullet.instances[i];
					if (movesMade % potentiallyFatalTurnInverval == 0)
						bullet.Act ();
				}
				for (int i = 0; i < Enemy.instances.Length; i ++)
				{
					Enemy enemy = Enemy.instances[i];
					if (enemy.trs.position == trs.position)
					{
						enemy.TakeDamage ();
						if (enemy == null)
							i --;
						else if (movesMade % potentiallyFatalTurnInverval == 0)
							enemy.Act ();
					}
					else if (movesMade % potentiallyFatalTurnInverval == 0)
						enemy.Act ();
				}
				turnIndicators[movesMade % potentiallyFatalTurnInverval].SetActive(false);
				movesMade ++;
				turnIndicators[movesMade % potentiallyFatalTurnInverval].SetActive(true);
				autoWaitTimer += timeUntilAutoWait;
				foreach (KeyValuePair<string, Achievement> keyValuePair in Achievement.instancesDict)
				{
					TimeAchievement timeAchievement = keyValuePair.Value as TimeAchievement;
					if (timeAchievement != null && autoWaitTimer >= (float) timeAchievement.time)
						timeAchievement.Receive ();
				}
				if (onActed != null)
					onActed ();
			}
			previousMove = move;
			previousWaitInput = waitInput;
		}

		public override void Death ()
		{
			_SceneManager.instance.RestartSceneWithoutTransition ();
		}
	}
}