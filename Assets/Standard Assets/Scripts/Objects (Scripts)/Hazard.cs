using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GridGame
{
	public class Hazard : MonoBehaviour
	{
		public Transform trs;
		public int damage;
		public SpriteRenderer spriteRenderer;
		public int becomeDangerousDelay;
		public GameObject[] destroyDelayIndicators = new GameObject[0];
		public static List<Hazard> instances = new List<Hazard>();
		int currentDestroyDelayIndicator = -1;

		void OnEnable ()
		{
			spriteRenderer.color = spriteRenderer.color.SetAlpha(1f / (becomeDangerousDelay + 1));
			instances.Add(this);
		}

		public void Act ()
		{
			spriteRenderer.color = spriteRenderer.color.AddAlpha(1f / (becomeDangerousDelay + 1));
			if (spriteRenderer.color.a >= .9f)
			{
				if (trs.position == Player.instance.trs.position)
					Player.instance.TakeDamage (damage);
				if (currentDestroyDelayIndicator >= 0)
					DestroyImmediate(destroyDelayIndicators[currentDestroyDelayIndicator]);
				else
					spriteRenderer.enabled = false;
				currentDestroyDelayIndicator ++;
				if (currentDestroyDelayIndicator == destroyDelayIndicators.Length)
					DestroyImmediate(gameObject);
				else
					destroyDelayIndicators[currentDestroyDelayIndicator].SetActive(true);
			}
		}

		void OnDisable ()
		{
			instances.Remove(this);
		}

		public bool WouldKillPlayerAtPosition (Vector2 position)
		{
			return spriteRenderer.color.a >= 1f - 1f / (becomeDangerousDelay + 1) - .1f && (Vector2) trs.position == position;
		}
	}
}